To study for my own sake/curiosity:
* study override keyword.
* does I have to declare a public empty constructor in order to get a defaut empty public constructor
  * (If no declaration at all is the default constructor public .. by default)
* study the constructor techniques to set attributes on the fly (taught by ambrou)
  * Blibli:Blibli() : myAttribute(myParameter)
  * How does it work really ?
  * What is it good for ? bad for ?
  * In French Ambrou said a professor used the acronym "Zim (Zone d'initialisation des membres)", in english MIL (member initialization list)
    * @see link to study #4


* Creating a console project by default creates this line "QCoreApplication a(argc, argv);"
  * What is this for in "Qt logic"
  * get more knowledge about it
  * even more with the need to instanciate a QGuiApplication here
  * as for the QPainter->drawText() not to crash in the library project "LogoJoy"

* Why is a non "pure virtual" method (=0 at the end) results in "vtable reference problem" 
  * message quite unclear
  * What is the vtable ??

*   auto foundPossibilites = std::find_if(possibleFonts.begin(), possibleFonts.end()
                                            , [&fontFamily](const QFont& scannedFont)
                                                {
                                                    return scannedFont.family() == fontFamily;
                                                });
  * I don't know how to come back from the result (Qvector::iterator // QFont*)  .. to ..  (QFont //  QVector<QFont>) easily

* deleting object of polymorphic class type ‘LogoJoy’ which has non-virtual destructor might cause undefined behaviour [-Wdelete-non-virtual-dtor]
   * yeah ... is your sister polymorphic too ?


Links to study:
* #1 https://blog.smartbear.com/c-plus-plus/c11-tutorial-lambda-expressions-the-nuts-and-bolts-of-functional-programming/
* #2 https://marcmutz.wordpress.com/effective-qt/containers/
* #3 http://www.embeddeduse.com/2016/05/13/simplifying-loops-with-cpp11/#ssec-find-if
* #4 
  * https://stackoverflow.com/questions/1711990/what-is-this-weird-colon-member-syntax-in-the-constructor
    * (2nd response.. long and precise, like when you HAVE TO use a MIL)
  * http://en.cppreference.com/w/cpp/language/initializer_list
  * https://isocpp.org/wiki/faq/ctors#init-lists
* #5 https://stackoverflow.com/questions/8130066/static-member-functions-error-how-to-properly-write-the-signature

Books 
* https://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list



```C++
	class ISshSession
	{
	public:
		virtual void connectTo(const std::wstring & strIpAddress) = 0;
	protected:
		ISshSession() = default;
		~ISshSession() = default;
	};
```

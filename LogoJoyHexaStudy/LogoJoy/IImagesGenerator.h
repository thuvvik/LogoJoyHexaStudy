#ifndef IIMAGESGENERATOR_H
#define IIMAGESGENERATOR_H

#include <QVector>
#include <QImage>

class IImagesGenerator
{
public:
    /**
     * this method generates all combinations for registered tastes
     *
     * @brief demonstrateGoodTaste
     * @return a list of images
     */
    virtual QVector<QImage> demonstrateGoodTaste() = 0;

protected:
    virtual ~IImagesGenerator() = default;
};

#endif // IIMAGESGENERATOR_H

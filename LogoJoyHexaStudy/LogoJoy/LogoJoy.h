#ifndef LOGOJOY_H
#define LOGOJOY_H

#include "logojoy_global.h"

#include "ITastesIndicator.h"
#include "IImagesGenerator.h"
#include "IFontsProvider.h"

class LOGOJOYSHARED_EXPORT LogoJoy : public ITastesIndicator, public IImagesGenerator
{
public:
    /**
     * @brief LogoJoy
     */
    LogoJoy(IFontsProvider& fontsProvider);
    LogoJoy(const LogoJoy& other);
    ~LogoJoy() = default;

//#- start of methods from ITastesIndicator
    /**
     * From "ITastesIndicator" interface
     *
     * @brief getPossibleFonts get available fonts to choose some from
     * @return available fonts
     */
    QVector<QFont> getPossibleFonts() override;
    /**
     * From "ITastesIndicator" interface
     *
     * @brief selectFonts indicate a taste for up to 5 fonts
     * @param chosenFonts the fonts we want to use later on
     */
    void selectFonts(const QVector<QFont> chosenFonts) override;
    /**
     * From "ITastesIndicator" interface
     *
     * @brief setMainText declare a text as the "main one", like a brand for example
     * @param mainText text to display as the most visible
     */
    void setMainText(const QString mainText) override;
//#- end of methods from ITastesIndicator


//#- start of methods from IImagesGenerator
    /**
     * this method generates all combinations for registered tastes
     *
     * @brief demonstrateGoodTaste
     * @return a list of images
     */
    QVector<QImage> demonstrateGoodTaste() override;
//#- end of methods from IImagesGenerator

private:
    QVector<QFont> _chosenFonts;
    QString _mainText;
    IFontsProvider& _fontsProvider;
};

#endif // LOGOJOY_H

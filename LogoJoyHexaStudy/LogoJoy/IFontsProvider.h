#ifndef IFONTSPROVIDER_H
#define IFONTSPROVIDER_H

// Qt
#include <QVector>
#include <QFont>
#include <QDebug>



class IFontsProvider
{
public:
    /**
     * @brief obtainFonts
     * @return
     */
    virtual QVector<QFont> obtainFonts() = 0;

protected:
    /**
     * @brief default destructor
     */
    virtual ~IFontsProvider(){ qDebug() << "Passage"; };
};
#endif // IFONTSPROVIDER_H

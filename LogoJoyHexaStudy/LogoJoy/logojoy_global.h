#ifndef LOGOJOY_GLOBAL_H
#define LOGOJOY_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LOGOJOY_LIBRARY)
#  define LOGOJOYSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LOGOJOYSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LOGOJOY_GLOBAL_H

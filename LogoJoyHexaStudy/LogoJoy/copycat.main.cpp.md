#include <QCoreApplication>
#include <QtSvg>
#include <QtSvg/QSvgRenderer>
#include <QtGui/QImage>
#include <QtGui/QPainter>
//#include <QFontDatabase>
#include <QTextStream>

// http://www.qtcentre.org/threads/55458-QSvgRenderer-onto-QImage-with-antialiasing
// https://stackoverflow.com/questions/37877465/text-is-not-antialiased-while-using-qpainterdrawtext
// http://www.qtcentre.org/threads/57502-How-to-make-svg-painters-measure-text-correctly
// http://3adly.blogspot.fr/2013/04/qt-save-qpainter-output-in-svg-or-image.html


int main(int argc, char *argv[])
{

    QApplication app(argc, argv);
    //QStringList fm = QFontDatabase().families();
    //QTextStream out(stdout);
    //for(int i=0 ; i < fm.length() ; i++)
    //{
        // Load your SVG
        QSvgRenderer renderer(QString("/home/ambroupollo/projects/LogoJoyCopycat/resources/React-icon.svg"));

        // Prepare a QImage with desired characteritisc
        //QPixmap pixmap(320,240);
        //pixmap.fill(Qt::white);

        QSvgGenerator generator;
         generator.setFileName("/home/ambroupollo/projects/LogoJoyCopycat/resources/result-purisa.svg");
         generator.setSize(QSize(640, 480));
         generator.setViewBox(QRect(0, 0, 640, 480));
         generator.setTitle(QString("SVG Generator Example Drawing"));
         generator.setDescription(QString("An SVG drawing created by the SVG Generator "
                                     "Example provided with Qt."));

        QPainter p3(&generator);
        p3.setRenderHints(QPainter::Antialiasing, true);
        p3.fillRect(QRect(0,0,640,480), Qt::white);
        renderer.render(&p3);

        //QImage image(500, 200, QImage::Format_ARGB32);
        //image.fill(0x00000000);
        // Get QPainter that paints to the image
        // QPainter painter(&image);

        QFont font;
        font.setPointSize(10);
        font.setFamily("Purisa");


        p3.setFont(font);
        p3.drawText(60, 60, "DAMN !!");
        p3.drawText(60, 120, "Awesome is good ");
        p3.end();
    //}

    // Font: Purisa

    // Save, image format based on file extension
    //pixmap.save("/home/ambroupollo/projects/LogoJoyCopycat/resources/result.svg");
}


/*
 *
 *  QSvgRenderer render(QString("test.svg"));
    pixmap.fill(Qt::white);
    QPainter p3(&pixmap);
    draw(p3);
    p3.end();
    pixmap.save("test_via_svg.png");


 QFont font;
    font.setPointSize(38);
    font.setFamily("Roboto");

    painter.setFont(font);

    painter.drawText(0, 60, "Google");

    QPainterPath textPath;
    textPath.addText(0, 140, font, "Google");
    painter.drawPath(textPath);
    */

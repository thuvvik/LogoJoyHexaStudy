#ifndef INDICATETASTES_H
#define INDICATETASTES_H

// Qt
#include <QVector>
#include <QFont>
#include <QImage>
#include <QString>

// LogoJoy
// @todo #include "ColorSet.h"

class ITastesIndicator
{
public:
    /**
     * @brief getPossibleFonts get available fonts to choose some from
     * @return available fonts
     */
    virtual QVector<QFont> getPossibleFonts() = 0;
    // @todo virtual QVector<QImage> getPossibleIcons() = 0;
    // @todo virtual QVector<QImage> getPossibleLayouts() = 0;
    // @todo virtual QVector<ColorSet> getPossibleColorSets() = 0;
    /**
     * @brief selectFonts indicate a taste for up to 5 fonts
     * @param chosenFonts the fonts we want to use later on
     */
    virtual void selectFonts(const QVector<QFont> chosenFonts) = 0;
    // @todo virtual void selectIcons(QVector<QImage> chosenIcons) = 0;
    // @todo virtual void selectLayouts(QVector<QImage> chosenLayouts) = 0;
    // @todo virtual void selectColorSets(QVector<ColorSet> chosenColorSets) = 0;

    /**
     * @brief setMainText declare a text as the "main one", like a brand for example
     * @param mainText text to display as the most visible
     */
    virtual void setMainText(QString mainText) = 0;
    // @todo virtual void setHeadline(QString headline);

protected:
    virtual ~ITastesIndicator() = default;
};

#endif // INDICATETASTES_H

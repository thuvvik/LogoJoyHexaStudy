#ifndef COLORSET_H
#define COLORSET_H

#include <QColor>

class ColorSet
{
public:
    ColorSet();
private:
    QColor _background;
    QColor _main;
    QColor _icon;
    QColor _slogan;
};

#endif // COLORSET_H

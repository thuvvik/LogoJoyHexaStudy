#include "LogoJoy.h"

// std
#include <algorithm> // std::merge, back_inserter

// Qt
#include <QSvgRenderer>
#include <QtGui/QPainter>

LogoJoy::LogoJoy(IFontsProvider& fontsProvider)
    : _fontsProvider(fontsProvider)
{

}

LogoJoy::LogoJoy(const LogoJoy& other)
    : _fontsProvider(other._fontsProvider)
{
}

QVector<QFont> LogoJoy::getPossibleFonts()
{
    // no right port for now !
    QFont fPurisa("Purisa");
    fPurisa.setPointSize(10);
    QFont fHelvetica("Helvetica");

    QVector<QFont> qtPossibilities;
    qtPossibilities << fPurisa;
    qtPossibilities << fHelvetica;

    QVector<QFont> fromfolder = this->_fontsProvider.obtainFonts();

    QVector<QFont> listPossibilities;
    std::merge(qtPossibilities.begin(), qtPossibilities.end()
               , fromfolder.begin(), fromfolder.end()
               , std::back_inserter(listPossibilities));

    return listPossibilities;
}

void LogoJoy::selectFonts(const QVector<QFont> chosenFonts)
{
    this->_chosenFonts = chosenFonts;
}

void LogoJoy::setMainText(const QString mainText)
{
    this->_mainText = mainText;
}

QVector<QImage> LogoJoy::demonstrateGoodTaste()
{
    QVector<QImage> results;

    for(QFont& currentFont : this->_chosenFonts)
    {
        // https://stackoverflow.com/questions/8551690/how-to-render-a-scaled-svg-to-a-qimage
        QSvgRenderer renderer(QString("/home/ambroupollo/projects/LogoJoyCopycat/resources/React-icon.svg"));
        QImage image(1200, 764, QImage::Format_ARGB32);

        QPainter painter(&image);
        painter.setRenderHints(QPainter::Antialiasing, true);
        painter.fillRect(QRect(0,0,1200,764), Qt::white);
        renderer.render(&painter);

        currentFont.setPointSize(22);
        painter.setFont(currentFont);
        painter.drawText(60, 60, "DAMN !!");
        painter.drawText(60, 120, "Awesome is good: The quick brown fox jumps over the lazy dog ");
        painter.drawText(60, 180, this->_mainText);
        painter.end();

        results.append(image);
    }

    return results;
}



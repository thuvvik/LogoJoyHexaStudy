#-------------------------------------------------
#
# Project created by QtCreator 2018-02-09T16:44:16
#
#-------------------------------------------------

TARGET = LogoJoyApplication
TEMPLATE = lib

DEFINES += LOGOJOYAPPLICATION_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        LogoJoyApplication.cpp

HEADERS += \
        LogoJoyApplication.h \
        logojoyapplication_global.h 

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LogoJoy/release/ -lLogoJoy
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LogoJoy/debug/ -lLogoJoy
else:unix: LIBS += -L$$OUT_PWD/../LogoJoy/ -lLogoJoy

INCLUDEPATH += $$PWD/../LogoJoy
DEPENDPATH += $$PWD/../LogoJoy

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LogoJoyFolderFonts/release/ -lLogoJoyFolderFonts
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LogoJoyFolderFonts/debug/ -lLogoJoyFolderFonts
else:unix: LIBS += -L$$OUT_PWD/../LogoJoyFolderFonts/ -lLogoJoyFolderFonts

INCLUDEPATH += $$PWD/../LogoJoyFolderFonts
DEPENDPATH += $$PWD/../LogoJoyFolderFonts

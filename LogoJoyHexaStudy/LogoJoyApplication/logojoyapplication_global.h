#ifndef LOGOJOYAPPLICATION_GLOBAL_H
#define LOGOJOYAPPLICATION_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LOGOJOYAPPLICATION_LIBRARY)
#  define LOGOJOYAPPLICATIONSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LOGOJOYAPPLICATIONSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LOGOJOYAPPLICATION_GLOBAL_H

#include "LogoJoyApplication.h"

#include "LogoJoyFolderFonts.h"

LogoJoyApplication::LogoJoyApplication(QString& fontsFolderPath)
    : logoJoyFolderfonts(LogoJoyFolderFonts(fontsFolderPath))
{
}

LogoJoyApplication::~LogoJoyApplication()
{
    delete this->ptrLogoJoy;
}

LogoJoy& LogoJoyApplication::getInstance()
{
    this->ptrLogoJoy = new LogoJoy(this->logoJoyFolderfonts);
    return *this->ptrLogoJoy;
}

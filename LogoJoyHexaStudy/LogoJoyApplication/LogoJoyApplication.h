#ifndef LOGOJOYAPPLICATION_H
#define LOGOJOYAPPLICATION_H

#include "logojoyapplication_global.h"

#include <QString>

#include "LogoJoy.h"
#include "LogoJoyFolderFonts.h"

class LOGOJOYAPPLICATIONSHARED_EXPORT LogoJoyApplication
{

public:
    LogoJoyApplication(QString& fontsFolderPath);
    ~LogoJoyApplication();
    LogoJoy& getInstance();
private:
    LogoJoyFolderFonts logoJoyFolderfonts;
    LogoJoy* ptrLogoJoy;
};

#endif // LOGOJOYAPPLICATION_H

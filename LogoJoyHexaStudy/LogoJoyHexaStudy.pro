TEMPLATE = subdirs

SUBDIRS += \
    LogoJoy \
    LogoJoyTests \
    LogoJoyConsole \
    LogoJoyFolderFonts \
    LogoJoyApplication \
    LogoJoyQmlGui

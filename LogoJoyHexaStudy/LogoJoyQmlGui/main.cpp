#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuickControls2>

#include "LogoJoyApplication.h"
#include "LogoJoy.h"

#include "ModelFontChoice.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QString fontsFolder("~/projects/LogoJoyHexaStudy/fonts/");
    LogoJoyApplication application(fontsFolder);
    LogoJoy mySystem(application.getInstance());

    QList<QObject*> bindedFontsList;
    for(const QFont &possibleFont : mySystem.getPossibleFonts())
    {
        for(int i=0; i< 10; i++)
        {
            bindedFontsList.append(new ModelFontChoice(possibleFont));
        }
    }

    QQmlApplicationEngine engine;    
    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("possibleFonts", QVariant::fromValue(bindedFontsList));

    const QUrl mainQml(QStringLiteral("qrc:/main.qml"));

    // Catch the objectCreated signal, so that we can determine if the root component was loaded
    // successfully. If not, then the object created from it will be null. The root component may
    // get loaded asynchronously.
    const QMetaObject::Connection connection = QObject::connect(
                &engine, &QQmlApplicationEngine::objectCreated,
                &app, [&](QObject *object, const QUrl &url) {
            if (url != mainQml)
            return;

            if (!object)
            app.exit(-1);
            else
            QObject::disconnect(connection);
}, Qt::QueuedConnection);

    engine.load(mainQml);

    return app.exec();
}

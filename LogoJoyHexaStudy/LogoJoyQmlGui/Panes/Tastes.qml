import QtQuick 2.4
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import "../Components"

GridLayout {
    id : grid
    rows: 3
    columns: 2
    anchors.fill: parent
    property double colMulti : grid.width / grid.columns
    property double rowMulti : grid.height / grid.rows
    function prefWidth(item){
        return colMulti * item.Layout.columnSpan
    }
    function prefHeight(item){
        return rowMulti * item.Layout.rowSpan
    }

    Text {
        Layout.row: 0
        Layout.column: 0
        text: qsTr("Available fonts to choose from")
    }

    FontsListView {
        Layout.row: 0
        Layout.column: 1
        Layout.preferredWidth  : grid.prefWidth(this)
        Layout.preferredHeight : grid.prefHeight(this)
    }
    Text {
        text: qsTr("Write your main text")
        Layout.row: 1
        Layout.column: 0
    }

    Text {
        text: qsTr("@todo text box")
        Layout.row: 1
        Layout.column: 1
    }
    Rectangle {
        id:filler
        Layout.row: 2
        Layout.column: 0
        Layout.columnSpan: 2
        color : "orange"

        Layout.fillHeight: true
        Layout.fillWidth: true
    }
}

import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 1.3
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.2
import "Components"
import "Panes"

Window {
    id: window
    visible: true
    width: 1024
    height: 768
    title: qsTr("Hello World")
    property string currentPane: "default"

    SplitView {
        anchors.fill: parent
        orientation: Qt.Horizontal
        Rectangle {
            id: rectRed
            Layout.minimumWidth: 24 // icon size
            width: ApplicationWindow.width / 8
            color: "red"

            ColumnLayout {
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.fillWidth: true
                anchors.fill: parent
                SplitMenuItem {
                    buttonText: qsTr("Menu")
                    imgSource: "qrc:/images/menu.png"
                    myState: "Menu"
                }
                SplitMenuItem {
                    buttonText: qsTr("Tastes")
                    imgSource: "qrc:/images/taste.png"
                    myState: "Tastes"
                }

                SplitMenuItem {
                    buttonText: qsTr("Execute")
                    imgSource: "qrc:/images/execute.png"
                    myState: "Execute"
                }
                Rectangle { // fill what's left of column as to push the precedent items up
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    color: rectRed.color
                }
            }
        }
        Rectangle {
            Layout.minimumWidth: (ApplicationWindow.width / 8)*7
            width: (ApplicationWindow.width / 8)*7
            color: "blue"

            PaneView {
                currentName: "Menu"
                currentColor: "green"
            }
            PaneView {
                currentName: "Tastes"
                Tastes {
                    anchors.fill: parent;
                }
            }
            PaneView {
                currentName: "Execute"
                currentColor: "magenta"
            }
        }
    }
}

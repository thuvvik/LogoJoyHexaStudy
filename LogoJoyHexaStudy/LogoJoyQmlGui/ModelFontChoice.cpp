#include "ModelFontChoice.h"

/*ModelFontChoice::ModelFontChoice(QObject *parent)
    : QObject(parent), _font(QFont("Helvetica"))
{

}*/
ModelFontChoice::ModelFontChoice(const QFont &font, QObject *parent)
    : QObject(parent), _font(font)
{

}

QString ModelFontChoice::getFamilyName()
{
    return this->_font.family();
}

QFont ModelFontChoice::getFont()
{
    return this->_font;
}

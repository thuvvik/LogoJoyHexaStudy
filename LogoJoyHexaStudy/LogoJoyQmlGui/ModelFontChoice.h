#ifndef MODELFONTCHOICE_H
#define MODELFONTCHOICE_H

#include <QObject>
#include <QFont>
#include <QString>

class ModelFontChoice : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString family READ getFamilyName)
    Q_PROPERTY(QFont font MEMBER _font)

public:
    /*explicit ModelFontChoice(QObject *parent = nullptr);*/
    ModelFontChoice(const QFont &font, QObject *parent = nullptr);

    QString getFamilyName();
    QFont getFont();

// signals:

//public slots:
//    selectionChanged(bool selected);

private:
    QFont _font;
//    bool _selected;
};

#endif // MODELFONTCHOICE_H

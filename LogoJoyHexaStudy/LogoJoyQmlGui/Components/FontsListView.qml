import QtQuick 2.4
import QtQuick.Controls 2.2

ListView {
    model: possibleFonts
    ScrollBar.vertical: ScrollBar { }

    delegate: Row {
        spacing: 10
        Text {
            text: family
            font.family: family
        }
    }
}

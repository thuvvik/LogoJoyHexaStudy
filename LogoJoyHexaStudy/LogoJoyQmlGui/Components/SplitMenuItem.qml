import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.2

ToolButton {
    property string buttonText: "Default Menu Text"
    property string imgSource: "qrc:/images/defaultimagesource.png"
    property string myState: "default"

    Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
    Layout.fillWidth: true
    Image {
      id: imgCurrent
      source: imgSource
      anchors.left: parent.left
      anchors.leftMargin: 5
      anchors.verticalCenter: parent.verticalCenter
    }
    Text {
        anchors.left: imgCurrent.right
        anchors.leftMargin: 10
        text:  buttonText
        anchors.verticalCenter: parent.verticalCenter
        horizontalAlignment: Text.AlignHCenter
    }

    onClicked: window.currentPane = myState;
}

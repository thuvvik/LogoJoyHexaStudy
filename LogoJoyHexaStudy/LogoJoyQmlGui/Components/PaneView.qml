import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.2

Pane {
    property string currentName: "default"
    property string currentColor: "grey"

    Layout.fillHeight: true
    Layout.fillWidth: true
    visible: window.currentPane === currentName;
    anchors.fill: parent
    Rectangle {
        anchors.fill: parent
        Layout.fillHeight: true
        Layout.fillWidth: true
        color: currentColor
    }
}

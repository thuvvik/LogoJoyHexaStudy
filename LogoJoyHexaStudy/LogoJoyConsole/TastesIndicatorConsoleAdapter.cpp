#include "TastesIndicatorConsoleAdapter.h"

// std
#include <algorithm> // std::copy_if

// Qt
#include <QVector>
#include <QFont>
#include <QImage>

void TastesIndicatorConsoleAdapter::askUserForItsTastes(ITastesIndicator& tastesIndicator)
{
    QTextStream out(stdout);
    QTextStream in(stdin);

    QVector<QFont> possibleFonts = tastesIndicator.getPossibleFonts();
    this->writeOn(QString("Available fonts to choose from :"), out);
    for(const QFont &possibleFont : possibleFonts)
    {
        this->writeOn(QString("    - " + possibleFont.family()), out);
    }

    QString fontFamily = this->ask("Which one do you want to select ?", out, in);

    QVector<QFont> chosenFonts;
    std::copy_if(possibleFonts.begin(), possibleFonts.end(), std::back_inserter(chosenFonts)
                                                , [&fontFamily](const QFont& scannedFont)
                                                    {
                                                        return scannedFont.family() == fontFamily;
                                                    });

    // get it back to tastesIndicator
    tastesIndicator.selectFonts(chosenFonts);

    QString mainText = this->ask("What is your main text ?", out, in);
    tastesIndicator.setMainText(mainText);

    // @todo this->writeOn(QString("Font not available, stopping process"), out);

}

void TastesIndicatorConsoleAdapter::writeOn(const QString& sentence,QTextStream& outputStream)
{
    outputStream << sentence << endl;
}

QString TastesIndicatorConsoleAdapter::ask(const QString& sentence,QTextStream& outputStream,QTextStream& inputStream)
{
    writeOn(sentence, outputStream);
    return inputStream.readLine();
}

void TastesIndicatorConsoleAdapter::registerImagesOnDisk(IImagesGenerator& imagesGenerator)
{
    QTextStream out(stdout);

    int index=0;
    for(QImage& currentImage : imagesGenerator.demonstrateGoodTaste())
    {
        this->writeOn("Passage", out);
        currentImage.save(QString("/tmp/" + QString::number(index) + ".png"));
        index++;
    }
}

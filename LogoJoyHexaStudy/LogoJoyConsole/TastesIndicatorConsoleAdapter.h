#ifndef TASTESINDICATORCONSOLEADAPTER_H
#define TASTESINDICATORCONSOLEADAPTER_H

// Qt
#include <QTextStream>
#include <QString>


// LogoJoy
#include "ITastesIndicator.h"
#include "IImagesGenerator.h"

/**
 * @brief The TastesIndicatorConsoleAdapter class
 */
class TastesIndicatorConsoleAdapter
{
public:
    /**
     * @brief TastesIndicatorConsoleAdapter
     * @param tastesIndicator
     */
    TastesIndicatorConsoleAdapter() = default;
    /**
     * @brief askUserForItsTastes
     */
    void askUserForItsTastes(ITastesIndicator& tastesIndicator);

    // helpers .. maybe move them elsewhere later on
    /**
     * @brief writeOn
     * @param sentence
     * @param outputStream
     */
    void writeOn(const QString& sentence,QTextStream& outputStream);
    /**
     * @brief ask
     * @param sentence
     * @param outputStream
     * @param inputStream
     * @return
     */
    QString ask(const QString& sentence,QTextStream& outputStream,QTextStream& inputStream);

    /**
     * @brief registerImagesOnDisk
     */
    void registerImagesOnDisk(IImagesGenerator& imagesGenerator);
};

#endif // TASTESINDICATORCONSOLEADAPTER_H

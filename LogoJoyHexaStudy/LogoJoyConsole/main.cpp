#include <QCoreApplication>

// Logojoy
#include "LogoJoy.h"
#include "ITastesIndicator.h"
// LogoJoy dependencies
#include "LogoJoyApplication.h"

// Console
#include "TastesIndicatorConsoleAdapter.h"
#include <QGuiApplication>

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    // initializing the core part of the hexagon, from the client
    QString fontsFolder("~/projects/LogoJoyHexaStudy/fonts/");
    LogoJoyApplication application(fontsFolder);
    LogoJoy mySystem(application.getInstance());

    // preparing interaction with the core
    TastesIndicatorConsoleAdapter adapter;
    // interact with the core to get the job done
    adapter.askUserForItsTastes(mySystem);
    adapter.registerImagesOnDisk(mySystem);

    return 0;
}

#ifndef LOGOJOYFOLDERFONTS_H
#define LOGOJOYFOLDERFONTS_H

#include "logojoyfolderfonts_global.h"

// Qt
#include <QString>

// LogoJoy
#include "IFontsProvider.h"

class LOGOJOYFOLDERFONTSSHARED_EXPORT LogoJoyFolderFonts : public IFontsProvider
{

public:
    LogoJoyFolderFonts(QString& folderPath);
    /**
     * @brief obtainFonts
     * @return
     */
    QVector<QFont> obtainFonts();
private:
    QString& _folderPath;
};

#endif // LOGOJOYFOLDERFONTS_H

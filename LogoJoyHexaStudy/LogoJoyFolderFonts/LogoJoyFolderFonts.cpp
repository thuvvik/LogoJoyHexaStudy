#include "LogoJoyFolderFonts.h"

#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QFontDatabase>


LogoJoyFolderFonts::LogoJoyFolderFonts(QString& folderPath)
    : _folderPath(folderPath)
{
    this->_folderPath.replace(QString("~"), QDir::homePath());
}

QVector<QFont> LogoJoyFolderFonts::obtainFonts()
{
    QVector<QFont> toreturn;

    QStringList namesFilter("*.ttf");
    QDirIterator it(this->_folderPath, namesFilter, QDir::Files);

    while(it.hasNext())
    {
        QFile file(it.next());
        QString filepath(it.filePath());
        int loadedFontID = QFontDatabase::addApplicationFont(filepath);
        if (loadedFontID > -1)
        {
            QString family = QFontDatabase::applicationFontFamilies(loadedFontID).at(0);
            QFont loaded(family);
            toreturn.append(loaded);
        }
    }

    return toreturn;
}
